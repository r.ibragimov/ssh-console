import {applyMiddleware, createStore} from "redux";
import thunk from "redux-thunk";
import {Provider} from "react-redux";
import React from "react";
import ReactDom from "react-dom";
import Console from "./components/Console";
import reducers from "./ducks/index";
import Tutorial from "./components/Tutorial";

require("./main.less");

const store = createStore(reducers, applyMiddleware(thunk));

ReactDom.render(
    <Provider store={store}>
        <div className="holder">
            <Console/>
            <Tutorial/>
        </div>
    </Provider>,
    document.getElementById("app")
);