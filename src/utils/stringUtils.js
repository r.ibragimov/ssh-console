export function removeCharAt(str, position) {
    if (position < 0) {
        return str;
    }
    return str.slice(0, position) + str.slice(position + 1);
}

export function insertCharAt(str, char, position) {
    if (position < 0) {
        return char + str;
    }
    return str.slice(0, position) + char + str.slice(position);
}

export function trim(str) {
    return str.trimLeft().trimRight();
}