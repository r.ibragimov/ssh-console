export function coalesce(...data) {
    for (let i = 0; i < data.length; i++) {
        const cur = data[i];
        if (cur !== undefined  && cur !== null && cur === cur) {
            return cur;
        }
    }
    return null;
}

export function ifNullOrUndefined(value, onTrue, onFalse) {
    if (value !== undefined  && value !== null && value === value) {
        return onFalse;
    }
    return onTrue;
}