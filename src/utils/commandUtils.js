import {addToHistory, setInputCommand, setLocation, setUsername} from "../ducks/console";
import {completeTask} from "../ducks/tutorial";

const commands = [
    {
        value: "ssh coolcastle.com",
        result: `
Welcome to Ubuntu 16.04.2 LTS (GNU/Linux 2.6.32-042stab123.9 x86_64)
        
* Documentation:  https://help.ubuntu.com
* Management:     https://landscape.canonical.com
* Support:        https://ubuntu.com/advantage`,
        prompt: {
            text: "user@coolcastle.com's password: ",
            isHidden: true,
            value: "123",
            invalidResult: "Permission denied, please try again."
        },
        callback: (dispatch) => {
            dispatch(setUsername("user@coolcastle.com"));
        }
    },
    {
        value: "ssh badman@coolcastle.com",
        result: `
Welcome to Ubuntu 16.04.2 LTS (GNU/Linux 2.6.32-042stab123.9 x86_64)
        
* Documentation:  https://help.ubuntu.com
* Management:     https://landscape.canonical.com
* Support:        https://ubuntu.com/advantage`,
        prompt: {
            text: "badman@coolcastle.com's password: ",
            isHidden: true,
            value: "321",
            invalidResult: "Permission denied, please try again."
        },
        callback: (dispatch) => {
            dispatch(setUsername("badman@coolcastle.com"));
        }
    },
    {
        value: "ssh coolcastle.com -l badman",
        result: `
Welcome to Ubuntu 16.04.2 LTS (GNU/Linux 2.6.32-042stab123.9 x86_64)
        
* Documentation:  https://help.ubuntu.com
* Management:     https://landscape.canonical.com
* Support:        https://ubuntu.com/advantage`,
        prompt: {
            text: "badman@coolcastle.com's password: ",
            isHidden: true,
            value: "321",
            invalidResult: "Permission denied, please try again."
        },
        callback: (dispatch) => {
            dispatch(setUsername("badman@coolcastle.com"));
        }
    },
    {
        value: "exit",
        result: `
logout
Connection to coolcastle.com closed.`,
        callback: (dispatch) => {
            dispatch(setUsername("user@local"));
        }
    },
    {
        value: "ssh-keygen -t rsa",
        result: `
The key fingerprint is:
SHA256:sTAIdMR4t+/uMLpM1AuCnasHhYWEWoukZheBQcVWTGY ribragimov@Rustams-MBP
The key's randomart image is:
+---[RSA 2048]----+
|+*B*BE           |
|oo+*=o.          |
|+=.oo.o..        |
|==oo ..o o       |
|+.= o ..S        |
|.  + . ..        |
| .. . +.         |
| ..o . o.        |
|..  +. oo        |
+----[SHA256]-----+`,
        prompt: {
            text: "Enter file in which to save the key (/Users/user/.ssh/id_rsa):",
            isHidden: false,
            value: "stone_rsa",
            invalidResult: "Try again."
        }
    },
    {
        value: "ssh-copy-id -i stone_rsa badman@coolcastle.com",
        result: `
Number of key(s) added:        1

Now try logging into the machine, with:   "ssh 'badman@coolcastle.com'"
and check to make sure that only the key(s) you wanted were added.`,
        prompt: {
            text: "badman@coolcastle.com's password: ",
            isHidden: true,
            value: "321",
            invalidResult: "Permission denied, please try again."
        },
        callback: (dispatch) => {
            dispatch(setUsername("badman@coolcastle.com"));
        }
    },
    {
        value: "ssh badman@coolcastle.com",
        result: `
Welcome to Ubuntu 16.04.2 LTS (GNU/Linux 2.6.32-042stab123.9 x86_64)
        
* Documentation:  https://help.ubuntu.com
* Management:     https://landscape.canonical.com
* Support:        https://ubuntu.com/advantage `,
        callback: (dispatch) => {
            dispatch(setUsername("badman@coolcastle.com"));
        }
    },
    {
        value: "ssh -p 69 badman@coolcastle.com",
        result: `
Welcome to Ubuntu 16.04.2 LTS (GNU/Linux 2.6.32-042stab123.9 x86_64)
        
* Documentation:  https://help.ubuntu.com
* Management:     https://landscape.canonical.com
* Support:        https://ubuntu.com/advantage `,
        callback: (dispatch) => {
            dispatch(setUsername("badman@coolcastle.com"));
        }
    },
];

export function processCommand(dispatch, getStore, curCommand) {
    const currentTask = getStore().tutorial.currentTask;
    const correctCommand = commands[currentTask];

    const curCommandTokens = curCommand.trim().split(" ").filter(i => i.length > 0);
    const correctCommandTokens = correctCommand.value.trim().split(" ");

    if (curCommandTokens.length === 0) {
        return;
    }

    if (curCommandTokens.length !== correctCommandTokens.length) {
        dispatch(addToHistory({
            type: "error",
            value: "-bash: -" + curCommand + ": incorrect number of arguments"
        }));
        return;
    }

    for (let i = 0; i < curCommandTokens.length; i++) {
        if (curCommandTokens[i] !== correctCommandTokens[i]) {
            dispatch(addToHistory({
                type: "error",
                value: "-bash: -" + curCommand + ": incorrect command"
            }));
            return;
        }
    }


    const {prompt} = correctCommand;
    if (correctCommand.prompt) {
        dispatch(setInputCommand({
            command: correctCommand.value,
            isHidden: prompt.isHidden,
            text: prompt.text
        }));
    } else {
        dispatch(addToHistory({
            type: "result",
            value: correctCommand.result,
        }));

        dispatch(setInputCommand(null));
        dispatch(completeTask(currentTask));
    }
}


export function processInputCommand(dispatch, getStore, cmd, input) {
    let cmdItem;


    for (let item of commands) {
        if (item.value === cmd) {
            cmdItem = item;
            break;
        }
    }

    dispatch(addToHistory({
        type: "innerInput",
        value: cmdItem.prompt.text + (cmdItem.prompt.isHidden ? "" : input)
    }));

    if (cmdItem.prompt.value === input) {
        const currentTask = getStore().tutorial.currentTask;

        dispatch(addToHistory({
            type: "result",
            value: cmdItem.result,
        }));

        dispatch(setInputCommand(null));
        dispatch(completeTask(currentTask));
        
        if (cmdItem.callback) {
            cmdItem.callback(dispatch);
        }
    } else {
        dispatch(addToHistory({
            type: "result",
            value: cmdItem.prompt.invalidResult,
        }));

        dispatch(setInputCommand({
            command: cmdItem.value,
            isHidden: cmdItem.prompt.isHidden,
            text: cmdItem.prompt.text
        }));
    }
}