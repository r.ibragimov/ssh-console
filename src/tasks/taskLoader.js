const tasks = [
    require("./1.md"),
    require("./2.md"),
    require("./3.md"),
    require("./4.md"),
    require("./5.md"),
    require("./6.md"),
    require("./7.md"),
    require("./8.md")
];


export function getTaskByIndex(index) {
    return tasks[index];
}


export function totalTasks() {
    return tasks.length;
}