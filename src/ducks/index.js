import {combineReducers} from "redux";
import console from "./console"
import tutorial from "./tutorial";

export default combineReducers({
    console,
    tutorial
});