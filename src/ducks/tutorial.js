import {createAction, handleActions} from "redux-actions";

const defaultState = {
    startTime: null,
    endTime: null,
    currentTask: 0,
    completedTasks: []
};

export const startTutorial = createAction("START_TUTORIAL");
export const endTutorial = createAction("END_TUTORIAL");
export const completeTask = createAction("COMPLETE_TASK");
export const openTask = createAction("OPEN_TASK");

const reducer = handleActions({
    [startTutorial]: (state, _) => ({...state, startTime: new Date()}),
    [endTutorial]: (state, _) => ({...state, endTime: new Date()}),
    [completeTask]: (state, {payload}) => {
        const newCurTask = state.currentTask + (payload === state.currentTask);
        if (state.completedTasks.indexOf(payload) < 0) {
            return {
                ...state,
                completedTasks: [...state.completedTasks, payload],
                currentTask: newCurTask
            }
        }
        return {...state, currentTask: newCurTask}
    },
    [openTask]: (state, {payload}) => ({...state, currentTask: payload})
}, defaultState);
export default reducer;