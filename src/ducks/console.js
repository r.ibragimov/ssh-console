import {createAction, handleActions} from "redux-actions";
import {insertCharAt, removeCharAt, trim} from "../utils/stringUtils";
import {clamp} from "../utils/numberUtils";
import {coalesce, ifNullOrUndefined} from "../utils/logicalUtils";
import {processCommand, processInputCommand} from "../utils/commandUtils";

const defaultState = {
    inputHistory: [],
    inHistoryPos: 0,
    allHistory: [],
    tabPressed: false,
    command: "",
    cursor: 0,
    data: {},
    username: "user@local",
    location: "~",
    inputCommand: null //{command:"sshPWD", isHidden: false, text:"type password"}
};

// Actions
const setData = createAction("SET_DATA", (key, value) => ({key, value}));
const setCommand = createAction("SET_COMMAND");
/// executeCommand(silent)
const executeCommand = createAction("EXECUTE_COMMAND");
const completeCommand = createAction("COMPLETE_COMMAND");
const editCommand = createAction("EDIT_COMMAND", (isDel) => isDel ? 0 : -1);
const moveCursor = createAction("MOVE_CURSOR", (isLeft) => isLeft ? -1 : 1);
const getInputHistory = createAction("GET_INPUT_HISTORY", (isUp) => isUp ? -1 : 1);
export const addToHistory = createAction("ADD_TO_HISTORY");
const setTabPressed = createAction("SET_TAB_PRESSED");
export const setUsername = createAction("SET_USERNAME");
export const setLocation = createAction("SET_LOCATION");
export const setInputCommand = createAction("SET_INPUT_COMMAND");

// Reducer
const reducer = handleActions({
    [setData]: (state, {payload: {key, value}}) => ({...state, data: {...state.data, [key]: value}}),
    [setCommand]: (state, {payload}) => {
        const newCommand = insertCharAt(state.command, payload, state.cursor);
        return {
            ...state,
            inputHistory: [...state.inputHistory.slice(0, -1), newCommand],
            command: newCommand,
            cursor: state.cursor + 1
        }
    },
    [executeCommand]: (state, {payload}) => {
        const res = {
            ...state,
            inHistoryPos: state.inputHistory.length,
            command: "",
            cursor: 0
        };
        if (payload) {
            res.inputHistory = [...state.inputHistory.slice(0, -1), ""];
        }else{
            res.inputHistory = [...state.inputHistory.slice(0, -1), trim(state.command), ""];
            res.allHistory = [...state.allHistory, {
                type: "cmd",
                username: state.username,
                location: state.location,
                value: state.command
            }];
        }
        return res;
    },
    [completeCommand]: (state, {payload}) => ({...state, command: payload, cursor: payload.length}),
    [editCommand]: (state, {payload}) => {
        const newCursor = state.cursor + payload;
        const newCommand = removeCharAt(state.command, newCursor);
        return {
            ...state,
            inputHistory: [...state.inputHistory.slice(0, -1), newCommand],
            command: newCommand,
            cursor: clamp(newCursor, 0, state.cursor)
        }
    },
    [moveCursor]: (state, {payload}) => ({
        ...state, cursor: clamp(state.cursor + payload, 0, state.command.length)
    }),
    [getInputHistory]: (state, {payload}) => {
        const newInHistoryPos = state.inHistoryPos + payload;
        const newCommand = state.inputHistory[newInHistoryPos];
        const cmd = coalesce(newCommand, state.command);
        return {
            ...state,
            command: cmd,
            inHistoryPos: clamp(newInHistoryPos, 0, state.inputHistory.length - 1),
            cursor: ifNullOrUndefined(newCommand, state.cursor, cmd.length),
        }
    },
    [addToHistory]: (state, {payload}) => ({...state, allHistory: [...state.allHistory, payload]}),
    [setTabPressed]: (state, {payload}) => ({...state, tabPressed: payload}),
    [setUsername]: (state, {payload}) => ({...state, username: payload}),
    [setLocation]: (state, {payload}) => ({...state, location: payload}),
    [setInputCommand]: (state, {payload}) => ({...state, inputCommand: payload}),
}, defaultState);
export default reducer;

// Thunks
export function handleKey(char, keyCode) {
    return (dispatch, getStore) => {
        const state = getStore();
        switch (keyCode) {
            case 13: { // enter
                const cmd = trim(state.console.command);
                if (state.console.inputCommand) {
                    dispatch(executeCommand(true));
                    processInputCommand(dispatch, getStore, state.console.inputCommand.command, cmd);
                } else if (cmd) {
                    dispatch(executeCommand());
                    processCommand(dispatch, getStore, cmd);
                }
                break;
            }
            case 9: { // tab
                dispatch(setTabPressed(true));
                //TODO: auto complete
                break;
            }
            case 8: { // backspace
                dispatch(editCommand(false));
                break;
            }
            case 46: { // delete
                dispatch(editCommand(true));
                break;
            }
            case 37:   // left
            case 39: { // right
                dispatch(moveCursor(keyCode === 37));
                break;
            }
            case 38:   // up
            case 40: { // down
                dispatch(getInputHistory(keyCode === 38));
                break;
            }
            default: {
                if (char && char.length === 1) {
                    dispatch(setCommand(char));
                }
            }
        }
        if (keyCode !== 9) {
            dispatch(setTabPressed(false));
        }
    }
}