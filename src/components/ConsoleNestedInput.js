import React from "react"
import ConsoleInput from "./ConsoleInput";

export default class ConsoleNestedInput extends ConsoleInput {
    render() {
        const {text, hidden} = this.props;
        let renderedValue = <span className="current">&nbsp;</span>;
        if (!hidden) {
            renderedValue = this._renderValue();
        }
        return (
            <div className="console-input">
                <div className="console-input_label">{text}</div>
                <div className="console-input_value">&nbsp;{renderedValue}</div>
            </div>
        )
    }
}