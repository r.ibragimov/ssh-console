import React from "react"

export default class Btn extends React.Component {
    constructor(props){
        super(props);
        this._handleClick = this._handleClick.bind(this);
    }

    _handleClick(e) {
        const {onClick, disabled} = this.props;
        if (!disabled && onClick) {
            onClick(e);
        }
    }

    render() {
        let className = "btn ";
        if (this.props.disabled) {
            className += "btn_disabled ";
        }
        className += this.props.className;
        return (
            <div onClick={this._handleClick} className={className}>{this.props.children}</div>
        )
    }
}