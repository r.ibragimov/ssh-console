import React from "react"

export default class ConsoleInput extends React.Component {
    _renderValue() {
        const {value, cursorPosition, editing} = this.props;
        if (!editing) {
            return value;
        }
        let result = [];

        const p1 = value.substr(0, cursorPosition);
        if (p1) {
            result.push(p1)
        }
        const p2 = value[cursorPosition];
        if (p2) {
            result.push(
                <span key={1} className="symbol current">{p2}</span>
            );
        }
        const p3 = value.substr(cursorPosition + 1);
        if (p3) {
            result.push(p3);
        }
        if (value.length <= cursorPosition) {
            result.push(<span key={cursorPosition} className="current">&nbsp;</span>);
        }
        return result;
    }

    render() {
        const {username, location} = this.props;
        const renderedValue = this._renderValue();
        return (
            <div className="console-input">
                <div className="console-input_username">{username}</div>
                :
                <div className="console-input_location">{location}</div>
                $
                <div className="console-input_value">&nbsp;{renderedValue}</div>
            </div>
        )
    }
}