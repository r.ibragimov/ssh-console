import React from "react"
import {connect} from "react-redux";
import ReactMarkdown from "react-markdown";
import {getTaskByIndex, totalTasks} from "../tasks/taskLoader";
import Btn from "./Btn";
import {endTutorial, openTask, startTutorial} from "../ducks/tutorial";
import {CodeBlock} from "./CodeBlock";

@connect(
    (state) => ({
        startTime: state.tutorial.startTime,
        endTime: state.tutorial.endTime,
        currentTask: state.tutorial.currentTask,
        completedTasks: state.tutorial.completedTasks
    }),
    (dispatch) => ({
        openTask: (p) => dispatch(openTask(p)),
        endTutorial: () => dispatch(endTutorial()),
        startTutorial: () => dispatch(startTutorial()),
    })
)
export default class Tutorial extends React.Component {
    constructor(props) {
        super(props);
        this._openNext = this._openNext.bind(this);
        this._openPrev = this._openPrev.bind(this);
    }

    componentDidMount(){
        this.props.startTutorial();
    }

    _openPrev() {
        const {openTask, currentTask} = this.props;
        if (currentTask > 0) {
            openTask(currentTask - 1);
        }
    }

    _openNext() {
        const {openTask, endTutorial, currentTask} = this.props;
        if (currentTask + 1 === totalTasks()) {
            endTutorial();
        } else {
            openTask(currentTask + 1);
        }
    }

    _scrollToTop() {
        if (this.tutorial) {
            this.tutorial.scrollTop = 0;
        }
    }

    componentWillReceiveProps(nextProps){
        if(this.props.currentTask !== nextProps.currentTask){
            this._scrollToTop();
            if (nextProps.currentTask === totalTasks()) {
                this.props.endTutorial();
            }
        }

    }

    render() {
        const {currentTask, completedTasks, startTime, endTime} = this.props;
        const taskMd = getTaskByIndex(currentTask);
        const total = totalTasks();
        if (endTime) {
            const time = Math.round((endTime - startTime) / 60000);
            return <div className="tutorial">
                <div className="tutorial-info">
                    <div className="tutorial-info_title">Completed!</div>
                    <div className="tutorial-info_content">Time: {time} min</div>
                </div>
            </div>
        }
        return (
            <div className="tutorial">
                <div className="tutorial-text scroll-styling" ref={
                    function (t) {
                        this.tutorial = t
                    }.bind(this)
                }>
                    <ReactMarkdown source={taskMd} renderers={{CodeBlock: CodeBlock}}/>
                </div>
                <div className="tutorial-panel">
                    <Btn className="tutorial-panel_btn" disabled={currentTask === 0} onClick={this._openPrev}>Prev</Btn>
                    <div className="tutorial-panel_pagination">{currentTask + 1} of {total}</div>
                    <Btn className="tutorial-panel_btn" disabled={completedTasks.indexOf(currentTask) < 0}
                         onClick={this._openNext}>Next</Btn>
                </div>
            </div>
        )
    }
}