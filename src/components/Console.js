import React from "react"
import {connect} from "react-redux";
import {handleKey} from "../ducks/console";
import ConsoleInput from "./ConsoleInput";
import ConsoleNestedInput from "./ConsoleNestedInput";

@connect(
    (state) => ({
        data: state.console,
        value: state.console.command,
        cursor: state.console.cursor,
        history: state.console.allHistory,
        username: state.console.username,
        location: state.console.location,
        inputCommand: state.console.inputCommand,
    }),
    (dispatch) => ({
        handleKey: (...data) => dispatch(handleKey(...data))
    })
)
export default class Console extends React.Component {
    constructor(prop) {
        super(prop);
        this._handleKeyDown = this._handleKeyDown.bind(this);
    }

    _handleKeyDown(event) {
        event.preventDefault();
        this.props.handleKey(event.key, event.keyCode);
    }

    componentWillMount() {
        window.addEventListener("keydown", this._handleKeyDown);
    }

    componentWillUnmount() {
        window.removeEventListener("keydown", this._handleKeyDown);
    }

    _buildHistory() {
        return this.props.history.map((h, i) => {
            if (h.type === "cmd") {
                return <ConsoleInput key={i} username={h.username} location={h.location} value={h.value}/>
            } else {
                return h.value.split("\n").map((item, i) => {
                    return (
                        <span key={i}>
                            {item}
                            <br/>
                        </span>
                    );
                });
            }
        });
    }

    _scrollToBottom() {
        if (this.console) {
            this.console.scrollTop = this.console.scrollHeight;
        }
    }

    render() {
        const {cursor, value, username, location, inputCommand} = this.props;
        const history = this._buildHistory();
        const InputClass = inputCommand ? ConsoleNestedInput : ConsoleInput;
        const {text, isHidden} = inputCommand||{};
        this._scrollToBottom();
        return (
            <div className="console scroll-styling" ref={
                function (c) {
                    this.console = c
                }.bind(this)
            }>
                {history}
                <InputClass username={username} location={location} editing={true}
                            cursorPosition={cursor}
                            value={value}
                            text={text}
                            hidden={isHidden}/>
            </div>
        )
    }
}