import React from "react";
import Prism from "prismjs";
import "prismjs/components/prism-bash";
import "prismjs/themes/prism-coy.css";

export function CodeBlock(props) {
    const lang = props.language || "bash";
    const html = Prism.highlight(props.literal, Prism.languages[lang]);
    const cls = "scroll-styling language-" + lang;

    return (
        <pre className={cls}>
          <code
              dangerouslySetInnerHTML={{__html: html}}
              className={cls}
          />
        </pre>
    )
}
