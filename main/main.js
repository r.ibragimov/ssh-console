const electron = require("electron");
const app = electron.app;
const BrowserWindow = electron.BrowserWindow;

app.on("window-all-closed", () => {
    if (process.platform !== "darwin") {
        app.quit();
    }
});

app.on("ready", () => {
    const mainWindow = new BrowserWindow({
        width: 800,
        height: 400,
        show: false,
        webPreferences: {
            devTools: false
        }
    });
    mainWindow.webContents.on("did-finish-load", () => {
        mainWindow.show();
    });
    mainWindow.loadURL("file://" + __dirname + "/main.html");
});