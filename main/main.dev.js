const electron = require("electron");
const app = electron.app;
const BrowserWindow = electron.BrowserWindow;

app.on("window-all-closed", () => {
    if (process.platform !== "darwin") {
        app.quit();
    }
});

app.on("ready", () => {
    let mainWindow = new BrowserWindow({
        width: 800,
        height: 400,
        show: false,
        resizable: true,
    });
    mainWindow.webContents.on("did-finish-load", () => {
        mainWindow.show();
    });
    mainWindow.loadURL("file://" + __dirname + "/main.dev.html");
    mainWindow.openDevTools();
});