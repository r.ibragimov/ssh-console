const path = require("path");
const webpack = require("webpack");

module.exports = function (env) {
    return {
        entry: {
            app: ["webpack/hot/dev-server", "./src/app.js"]
        },
        output: {
            path: __dirname + "/main",
            filename: "bundle.js",
            publicPath: "http://localhost:8888/"
        },
        devServer: {
            contentBase: __dirname + "/main",
            publicPath: "http://localhost:8888/"
        },
        devtool: "source-map",
        module: {
            loaders: [
                {
                    test: /\.js$/,
                    loader: "babel-loader",
                    query: {
                        plugins: ["transform-decorators-legacy", "transform-object-rest-spread"],
                        presets: ["es2015", "react"]
                    },
                    exclude: /node_modules/,
                },
                {
                    test: /\.css$/,
                    loader: "style-loader!css-loader"
                },
                {
                    test: /\.less$/,
                    loader: "style-loader!css-loader!less-loader"
                },
                {
                    test: /\.md$/,
                    loader: "raw-loader"
                },
                {
                    test: /\.(png|woff|woff2|eot|ttf|svg|otf)$/,
                    loader: "url-loader?limit=100000"
                }
            ]
        },
        plugins: [
            new webpack.HotModuleReplacementPlugin()
        ]
    }
};